"""
Optimal interpolation classes
"""
import numpy as np
from scipy import linalg as la


##
# Likelihood functions
def log_marginal_likelihood(K, zd):
    
    N = zd.shape[0]

    #W = la.solve(Kdd, yd)
    # Cholesky needs some noise...
    try:
        L = la.cholesky(K+1e-7*np.eye(N))
    except:
        return -np.inf

    S1 = la.solve_triangular(L, zd, lower=True)
    S2 = la.solve_triangular(L.T, S1, lower=False)

    return -np.sum(np.log(np.diagonal(L))) - \
                0.5 * zd.dot(S2) - \
                0.5 * N * np.log(2*np.pi)

def log_marginal_likelihood_1d(xd, zd, sd, cov_func, cov_params):
    
    N = xd.shape[0]
    I = np.diag(np.ones((N)))
    K = cov_func(xd[:,None], xd[:,None].T, cov_params) + sd**2*I
    
    return log_marginal_likelihood(K, zd)

def log_marginal_likelihood_3d(xd, yd, td, zd, sd, cov_func, cov_params):
    
    N = xd.shape[0]
    I = np.diag(np.ones((N)))
    
    K = cov_func(xd[:,None], yd[:,None], td[:,None], xd[:,None].T, yd[:,None].T, td[:,None].T, cov_params) + sd**2*I
    
    return log_marginal_likelihood(K, zd)


###
# OI classes
class OptimalInterp1D(object):
    """
    Optimal interpolation in one dimension (usually time)
    
    """
    
    def __init__(self, xd, xm, sd, cov_func, cov_params):
        
        self.n = xd.shape[0]
        self.m = xm.shape[0]    
        self.xd = xd
        self.xm = xm
        
        self.cov_params = cov_params
        self.cov_func = cov_func

        # Compute the covariance functions
        Kmd = cov_func(xm[:,None], xd[:,None].T, cov_params)
        Kdd = cov_func(xd[:,None], xd[:,None].T, cov_params)
        
        # Calculate the weights
        self.w_md = self._calc_weights(Kdd, sd, Kmd.T)
        
        #self.L = la.cholesky(Kdd+noise)
        #self.Kmd = Kmd
        
    def __call__(self, yd):
        
        assert yd.shape[0] == self.n, ' first dimension in input data must equal '
        
        #a1 = la.solve_triangular(self.L, yd, )
        #alpha = la.solve_triangular(self.L.T, a1,)
        #print(alpha.shape, self.Kmd.T.shape)
        #return np.dot(self.Kmd, alpha)
        return np.dot(self.w_md.T, yd[:,:])
    
    def calc_err(self):
        Kmm = self.cov_func(self.xm[:,None], self.xm[:,None].T, self.cov_params)
        Kdm = self.cov_func(self.xd[:,None], self.xm[:,None].T, self.cov_params)
        V = Kmm - self.w_md.T.dot(Kdm)

        return np.diagonal(V)
    
    def neg_log_marginal_likelihood(self, xd, yd, sd, cov_func, cov_params):
        
        mu = self.__call__(yd)
        zd = yd - mu.ravel()
        return -log_marginal_likelihood_1d(xd, zd, sd, cov_func, cov_params)
    
    def _calc_weights(self, K, sd, Ks):
        # noise term 
        #I = np.diag(np.ones((self.n,)))
        I = np.eye(self.n)
        noise = sd**2*I
        
        return la.solve(K+noise+1e-8*I, Ks)
    

class OptimalInterp2D(object):
    """
    Optimal interpolation in two spatial dimensions
    
    
    """
    
    def __init__(self, xd, yd, xm, ym, sd, cov_func, cov_params):
        
        self.n = yd.shape[0]
        self.m = ym.shape[0]
        
        assert ym.shape == xm.shape
        assert xd.shape == yd.shape
        
        self.xd = xd
        self.yd = yd
        self.xm = xm
        self.ym = ym
        self.cov_params = cov_params
        self.cov_func = cov_func

        # Compute the covariance functions
        Kmd = cov_func(xm[:,None], ym[:,None], xd[:,None].T, yd[:,None].T, cov_params)
        Kdd = cov_func(xd[:,None], yd[:,None], xd[:,None].T, yd[:,None].T, cov_params)

        # noise term 
        I = np.diag(np.ones((self.n,)))
        noise = sd**2*I

        # Calculate the weights
        self.w_md = np.linalg.solve(Kdd + noise , Kmd.T)

        
    def __call__(self, yd):
        
        assert yd.shape[0] == self.n, ' first dimension in input data must equal '
        
        return self.w_md.T.dot(yd[:,:])
        
    def calc_err(self):
        Kmm = self.cov_func(self.xm[:,None], self.ym[:,None], self.xm[:,None].T, self.ym[:,None].T, self.cov_params)
        Kdm = self.cov_func(self.xd[:,None], self.yd[:,None], self.xm[:,None].T, self.ym[:,None].T, self.cov_params)
        V = Kmm - self.w_md.T.dot(Kdm)

        return np.diagonal(V)
 
class OptimalInterp3D(object):
    """
    Optimal interpolation in three dimensions
    
    
    """
    
    def __init__(self, xd, yd, td, xm, ym, tm, sd, cov_func, cov_params):
        
        self.n = yd.shape[0]
        self.m = ym.shape[0]
        
        assert ym.shape == xm.shape
        assert xd.shape == yd.shape
        
        self.xd = xd
        self.yd = yd
        self.td = td
        self.xm = xm
        self.ym = ym
        self.tm = tm

        self.cov_params = cov_params
        self.cov_func = cov_func

        # Compute the covariance functions
        Kmd = cov_func(xm[:,None], ym[:,None], tm[:,None], xd[:,None].T, yd[:,None].T, td[:,None].T, cov_params)
        Kdd = cov_func(xd[:,None], yd[:,None], td[:,None], xd[:,None].T, yd[:,None].T, td[:,None].T, cov_params)

        # noise term 
        I = np.diag(np.ones((self.n,)))
        noise = sd**2*I

        # Calculate the weights
        self.w_md = np.linalg.solve(Kdd + noise , Kmd.T)

        
    def __call__(self, yd):
        
        assert yd.shape[0] == self.n, ' first dimension in input data must equal '
        
        return self.w_md.T.dot(yd[:,:])
        
    def calc_err(self):
        Kmm = self.cov_func(self.xm[:,None], self.ym[:,None], self.tm[:,None], self.xm[:,None].T, self.ym[:,None].T, self.tm[:,None].T, self.cov_params)
        Kdm = self.cov_func(self.xd[:,None], self.yd[:,None], self.td[:,None], self.xm[:,None].T, self.ym[:,None].T, self.tm[:,None].T, self.cov_params)
        V = Kmm - self.w_md.T.dot(Kdm)
        return np.diagonal(V)
 
    def neg_log_marginal_likelihood(self, xd, yd, td, zd, sd, cov_func, cov_params):
        return -log_marginal_likelihood_3d(xd, yd, td, zd, sd, cov_func, cov_params)