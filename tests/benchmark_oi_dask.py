#!/usr/bin/env python
# coding: utf-8

# In[1]:


import time

import numpy as np
from scipy.special import kv as K_nu
from scipy.special import gamma
import dask
import dask.array as da

# Dask distributed/mpi stuff
import warnings
warnings.simplefilter(action='ignore', category=da.core.PerformanceWarning)

from dask_mpi import initialize
initialize()

from distributed import Client
client = Client()



#print(client)

class OptimalInterpDask(object):
    """
    Optimal interpolation using dask to do the heavy lifting
    
    """
    
    chunksize=1000
    threshold=1e-15
    compute=False
    
    def __init__(self, xd, xm, sd, cov_func, cov_params, **kwargs):
        
        self.__dict__.update(kwargs)
        
        assert xd.ndim==2
        
        self.N, self.D = xd.shape
        self.M, D = xm.shape
        
        # Chunks must divide up the array exactly
        assert self.N%self.chunksize==0

        self.xd = da.from_array(xd, chunks=(self.chunksize,1))
        self.xm = da.from_array(xm, chunks=(self.chunksize,1))
        
        self.sd = sd
        self.cov_func = cov_func
        self.cov_params = cov_params
        
        self.Kmd, self.Kdd = self._calc_cov(cov_func, cov_params)
        
        self.L, self.w_md = self._calc_weights(self.Kdd, self.sd, self.Kmd)
        
    def __call__(self, yd):
        
        assert yd.shape[0] == self.N, ' first dimension in input data must equal '
        
        yd = da.from_array(yd, chunks=(self.chunksize,1))

        v = da.linalg.solve_triangular(self.L, yd, lower=True)
        alpha = da.linalg.solve_triangular(self.L.T, v, lower=False)
        
        return self.Kmd.dot(alpha)
        
    
    def _calc_cov(self, cov_func, cov_params):
        # Compute the covariance functions
        Kmd = cov_func(self.xm, self.xd, cov_params)
        Kdd = cov_func(self.xd, self.xd, cov_params)
        return Kmd, Kdd
    
    def _calc_weights(self, Kdd, sd, Kmd):
        
        # Calculate the cholesky factorization
        w_md = None
        
        sigI = da.eye(self.N,chunks=self.chunksize)*(sd+1e-7)
        
        L = da.linalg.cholesky(Kdd+sigI, lower=True)
        
        if self.compute:
            return L.persist(), w_md
        else:
            return L, w_md

    def calc_err(self, diag=True):

        Kmm = self.cov_func(self.xm, self.xm, self.cov_params)
        Kdm = self.cov_func(self.xd, self.xm, self.cov_params)
        
        V = Kmm - self.w_md.T.dot(Kdm)

        if diag:
            return V.diagonal()
        else:
            return V
        
    def sample_posterior(self, yd, samples):
        
        # Predict the mean
        ymu = self.__call__(yd)

        # Predict the covariance
        Σ = self.calc_err(diag=False)
        
        L = da.linalg.cholesky(Σ, lower=True).persist()
        
        myrand = da.random.normal(size=(self.N,samples)) 
        
        return ymu + L.dot(myrand)
        
    def sample_prior(self, samples):
        
        myrand = da.random.normal(size=(self.N,samples)) 
        
        return self.L.dot(myrand)

    def log_marg_likelihood(self, yd):
        
        yd = da.from_array(yd, chunks=(self.chunksize,1))
        
        logdet = 2*da.sum(da.log(da.diagonal(self.L)))
        
        v = da.linalg.solve_triangular(self.L, yd, lower=True)
        alpha = da.linalg.solve_triangular(self.L.T, v, lower=False)
        
        qdist = da.dot(yd.T, alpha)[0,0]
        
        fac = self.N * da.log(2*np.pi)
        
        return -0.5*(logdet + qdist + fac)
      
            


# In[2]:



def matern52(x,xpr,l):
    fac1 = 5*(x-xpr)*(x-xpr)
    fac2 = np.sqrt(fac1)
    return (1 + fac2/l + fac1/(3*l*l) )*np.exp(-fac2/l)

def matern32(x,xpr,l):
    fac1 = 3*(x-xpr)*(x-xpr)
    fac2 = np.sqrt(fac1)
    return (1 + fac2/l)*np.exp(-fac2/l)

def cosine(x,xpr,l):
    return np.cos(np.pi*np.abs(x-xpr)/(l*l))

def expquad(x, xpr, l):
    return np.exp(-(x-xpr)*(x-xpr)/(2*l*l))

def expquad2(x, x2, xpr, xpr2, lxy):
    return np.exp(-(x-xpr)*(x2-xpr2)*lxy)

def matern_general(dx, eta, nu, l):
    
    cff1 = np.sqrt(2*nu)*dx/l
    K = np.power(eta, 2.) * np.power(2., 1-nu) / gamma(nu)
    K *= np.power(cff1, nu)
    K *= K_nu(nu,cff1)
    
    K[np.isnan(K)] = np.power(eta, 2.)
    
    return K


# In[3]:


def spectral_mixture_kernel(x, xpr, params):
    eta, l, T = params
    #T = 2*np.sqrt(T)
    
    return eta**2 *expquad(x[:,0,None], xpr[:,0,None].T, l)*        cosine(x[:,0,None], xpr[:,0,None].T, T)

def spectral_mixture_kernel_old(x, xpr, params):
    eta, l, T = params    
    return eta**2 *expquad(x, xpr, l)*        cosine(x, xpr, T)

#2D Kernel
def spectral_mixture_kernel_2d(X1, X2, params):
    
    assert X1.shape[1] == 2
    
    eta, decay_x, l_x, decay_y, l_y =  params
    l_x = np.sqrt(l_x)
    l_y = np.sqrt(l_y)
    
    #print(X1.shape)
    #print(cov.expquad(X1[:,0,None], X2[:,0,None].T, decay_x).shape)
    return eta**2 *expquad(X1[:,0,None], X2[:,0,None].T, decay_x)*        cosine(X1[:,0,None], X2[:,0,None].T, l_x) *        expquad(X1[:,1,None], X2[:,1,None].T, decay_y)*        cosine(X1[:,1,None], X2[:,1,None].T, l_y)

def matern_2d(X1, X2, params):
    
    assert X1.shape[1] == 2
    
    eta, nu, l_x, l_y =  params
    
    dx = np.sqrt(np.power(X1[:,0,None]-X2[:,0,None].T,2))
    dy = np.sqrt(np.power(X1[:,1,None]-X2[:,1,None].T,2))

    return eta**2 * matern_general(dx, 1, nu, l_x) *        matern_general(dy, 1, nu, l_y)


# In[17]:


#%%time

def run_oi(nx, ny, chunksize):
    #########
    # Parameters
    noise = 0.1
    covfunc = spectral_mixture_kernel_2d
    covparams = (0.05, 120, 100., 60., 40.1)
    dx = 2.0
    #nx = 60
    #ny = 120
    #chunksize=1800
    #########

    N= nx*ny
    xin, yin = np.arange(0,dx*nx,dx) , np.arange(0,dx*ny,dx)
    xd, yd = np.meshgrid(xin, yin)
    xo = xd # np.linspace(-10*dx,dx*N+dx*10,N*2)
    yo = yd

    Xd = np.array([xd.ravel(),yd[:,None].ravel()]).T
    Xo = np.array([xo[:,None].ravel(),yo[:,None].ravel()]).T


    t0 = time.time()

    OIs = OptimalInterpDask(Xd, Xo, noise, covfunc, covparams, chunksize=chunksize, threshold=1e-8,
                           compute=True)


    yd = OIs.sample_prior(1).compute()
    t1 = time.time()

    yo = OIs(yd).compute()

    t2 = time.time()

    t_cholesky = t1-t0
    t_sample = t2-t1

    client.cancel(OIs)

    return N, OIs.Kdd.npartitions, t_cholesky, t_sample

###########################################################
print(72*'#')
print('Dask stuff above here...')
print(72*'#')



# Run a case
print("N, # chunks, t chol (s), t sample (s)")
print(run_oi(50, 100, 1000))
print(run_oi(50, 200, 1000))
print(run_oi(50, 200, 500))
print(run_oi(50, 200, 2000))

print(72*'#')
print('Dask stuff below here...')
print(72*'#')

client.close()
