"""
Main optimal interpolation classes
"""

import numpy as np
from scipy import linalg as la

###
# OI classes
class OptimalInterp1D(object):
    """
    Optimal interpolation in one dimension (usually time)
    
    """
    
    def __init__(self, xd, xm, sd, cov_func, cov_params):
        
        self.N = xd.shape[0]
        self.M = xm.shape[0]    
        self.xd = xd
        self.xm = xm
        
        self.sd = sd
        
        self.cov_params = cov_params
        self.cov_func = cov_func

        self.Kmd, self.Kdd = self._calc_cov(cov_func, cov_params)
        
        self.L, self.w_md = self._calc_weights(self.Kdd, self.sd, self.Kmd)
       
        
    def __call__(self, yd):
        
        assert yd.shape[0] == self.N, ' first dimension in input data must equal '
       
        return np.dot(self.w_md.T, yd[:,:])
    
    def _calc_cov(self, cov_func, cov_params):
        # Compute the covariance functions
        Kmd = cov_func(self.xm[:,None], self.xd[:,None].T, cov_params)
        Kdd = cov_func(self.xd[:,None], self.xd[:,None].T, cov_params) 

        return Kmd, Kdd
    
    def _calc_weights(self, Kdd, sd, Kmd):
        
        noise = sd**2*np.eye(self.N)
        # Calculate the cholesky factorization
        L = la.cholesky(Kdd+noise+1e-7*np.eye(self.N), lower=True)

        # Compute weights matrix
        w_md = la.cho_solve((L, True),  Kmd.T)
        
        return L, w_md


    def calc_err(self, diag=True):
        Kmm = self.cov_func(self.xm[:,None], self.xm[:,None].T, self.cov_params)
        Kdm = self.cov_func(self.xd[:,None], self.xm[:,None].T, self.cov_params)
        V = Kmm - self.w_md.T.dot(Kdm)

        if diag:
            return np.diagonal(V)
        else:
            return V
        
    def sample_posterior(self, yd, samples):
        
        # Predict the mean
        ymu = self.__call__(yd)

        # Predict the covariance
        Σ = self.calc_err(diag=False)

        return np.random.multivariate_normal(ymu.ravel(), Σ, samples)
    
    def sample_prior(self, samples):
        
        return np.random.multivariate_normal(np.zeros((self.N,)), self.Kdd, samples)

    
    def log_marg_likelihood(self, yd):
        
        logdet = 2*np.sum(np.log(np.diagonal(self.L)))
        
        v = la.cho_solve((self.L, True), yd )
        
        qdist = np.dot(yd.T, v)[0,0]
        
        fac = self.N * np.log(2*np.pi)
        
        return -0.5*(logdet + qdist + fac)
        
            
class OptimalInterp2D(OptimalInterp1D):
    
    def __init__(self, xd, yd, xm, ym, sd, cov_func, cov_params):
        
        self.yd = yd
        self.ym = ym
        OptimalInterp1D.__init__(self,xd, xm, sd, cov_func, cov_params)


    def _calc_cov(self, cov_func, cov_params):
        # Compute the covariance functions
        Kmd = cov_func(self.xm[:,None], self.ym[:,None], self.xd[:,None].T, self.yd[:,None].T, cov_params)
        Kdd = cov_func(self.xd[:,None], self.yd[:,None], self.xd[:,None].T, self.yd[:,None].T, cov_params)

        return Kmd, Kdd
    
    def calc_err(self, diag=True):
        Kmm = self.cov_func(self.xm[:,None], self.ym[:,None],\
                self.xm[:,None].T, self.ym[:,None].T, self.cov_params)
        Kdm = self.cov_func(self.xd[:,None], self.yd[:,None],\
                self.xm[:,None].T, self.ym[:,None].T, self.cov_params)
        V = Kmm - self.w_md.T.dot(Kdm)

        if diag:
            return np.diagonal(V)
        else:
            return V


class OptimalInterp3D(OptimalInterp1D):
    
    def __init__(self, xd, yd, td, xm, ym, tm, sd, cov_func, cov_params):
        
        self.yd = yd
        self.ym = ym
        self.td = td
        self.tm = tm
        OptimalInterp1D.__init__(self,xd, xm, sd, cov_func, cov_params)


    def _calc_cov(self, cov_func, cov_params):
        # Compute the covariance functions
        Kmd = cov_func(self.xm[:,None], self.ym[:,None], self.tm[:,None],\
                self.xd[:,None].T, self.yd[:,None].T, self.td[:,None].T, cov_params)
        Kdd = cov_func(self.xd[:,None], self.yd[:,None], self.td[:,None],\
                self.xd[:,None].T, self.yd[:,None].T, self.td[:,None].T, cov_params)

        return Kmd, Kdd
    
    def calc_err(self, diag=True):
        Kmm = self.cov_func(self.xm[:,None], self.ym[:,None], self.tm[:,None],\
                self.xm[:,None].T, self.ym[:,None].T, self.tm[:,None].T, self.cov_params)
        Kdm = self.cov_func(self.xd[:,None], self.yd[:,None], self.td[:,None],\
                self.xm[:,None].T, self.ym[:,None].T, self.tm[:,None].T, self.cov_params)
        V = Kmm - self.w_md.T.dot(Kdm)

        if diag:
            return np.diagonal(V)
        else:
            return V


