import numpy as np
from scipy import linalg as la

class OptimalInterpScipy(object):
    """
    Optimal interpolation using dask to do the heavy lifting
    
    """
    
    mean_func = None
    mean_params = ()
    mean_kwargs = {}
    P=1 # Number of output dimensions
    
    cov_kwargs = {}
    
    is_toeplitz = False
    
    def __init__(self, xd, xm, sd, cov_func, cov_params, **kwargs):
        
        self.__dict__.update(kwargs)
        
        assert xd.ndim==2
        
        self.N, self.D = xd.shape
        self.M, D = xm.shape
        
        self.N = self.N*self.P
        self.M = self.M*self.P

        self.xd = xd
        self.xm = xm
        
        self.sd = sd
        self.cov_func = cov_func
        self.cov_params = cov_params
        
        # Evaluate the covariance functions
        self.Kmd, self.Kdd = self._calc_cov(cov_func, cov_params)
        
        # Evaluate the mean function
        if self.mean_func is None:
            self.mu_d = 0.
            self.mu_m = 0.
        else:
            self.mu_d = self.mean_func(self.xd, self.mean_params, **self.mean_kwargs)
            self.mu_m = self.mean_func(self.xm, self.mean_params, **self.mean_kwargs)
        
        # Calculate the cholesky of Kdd for later use
        self.L, self.w_md = self._calc_weights(self.Kdd, self.sd, self.Kmd)
        
    def __call__(self, yd):
        
        assert yd.shape[0] == self.N, ' first dimension in input data must equal '
        
        if self.is_toeplitz:
            alpha = la.solve_toeplitz(L, yd - self.mu_d)
        else:
            alpha = la.cho_solve((self.L, True), yd - self.mu_d)
        
        return self.mu_m + self.Kmd.dot(alpha)
        
    
    def _calc_cov(self, cov_func, cov_params):
        # Compute the covariance functions
        Kmd = cov_func(self.xm, self.xd.T, cov_params, **self.cov_kwargs)
        Kdd = cov_func(self.xd, self.xd.T, cov_params, **self.cov_kwargs)
        return Kmd, Kdd
    
    def _calc_weights(self, Kdd, sd, Kmd):
         
        if self.is_toeplitz:
            c = Kdd[:,0]
            c[0] += (sd**2+1e-7)
            #r = Kdd[0,:]
            #r[0] += (sd**2+1e-7)
            #L = (c,r)
            L=c
        else:
            # Calculate the cholesky factorization
            L = la.cholesky(Kdd+(sd**2+1e-7)*np.eye(self.N), lower=True)

        # Compute weights matrix (these consume a lot of memory)
        # This step is actually fast so not worth saving it in memory
        #w_md = la.cho_solve((L, True),  Kmd.T)
        w_md = None

        return L, w_md

    def _calc_err(self, diag=True):

        Kmm = self.cov_func(self.xm, self.xm.T, self.cov_params, **self.cov_kwargs)
        Kdm = self.cov_func(self.xd, self.xm.T, self.cov_params, **self.cov_kwargs)
        
        if self.is_toeplitz:
            v = la.solve_toeplitz(self.L,  Kdm)
        else:
            v = la.cho_solve((self.L, True),  Kdm)
        
        V = Kmm - v.T.dot(Kdm)
        
        if diag:
            return np.diag(V)
        else:
            return V
        
    def prior(self, samples=1):
        return self.sample_prior(samples)
    
    def conditional(self, yd, samples=1):
        return self.sample_posterior(yd, samples)
        
    def sample_posterior(self, yd, samples):
        
        if self.is_toeplitz:
            raise NotImplementedError
            
        # Predict the mean
        ymu = self.__call__(yd)

        # Predict the covariance
        Σ = self._calc_err(diag=False)
        
        myrand = np.random.normal(size=(self.M,samples))
        L = la.cholesky(Σ+1e-7*np.eye(self.M), lower=True)
        
        return ymu + L.dot(myrand)
        
        #return np.random.multivariate_normal(ymu.ravel(), Σ, samples).T
        
    def sample_prior(self, samples):
        if self.is_toeplitz:
            raise NotImplementedError
        
        myrand = np.random.normal(size=(self.N,samples)) 
        
        return self.mu_d + self.L.dot(myrand)

    def log_marg_likelihood(self, yd):
                
        logdet = 2*np.sum(np.log(np.diagonal(self.L)))
        
        if self.is_toeplitz:
            print('Using toeplitz solve')
            alpha = la.solve_toeplitz(self.L,  yd - self.mu_d)
        else:
            alpha = la.cho_solve((self.L, True), yd - self.mu_d)
        
        qdist = np.dot( (yd-self.mu_d).T, alpha)[0,0]
        
        fac = self.N * np.log(2*np.pi)
        
        return -0.5*(logdet + qdist + fac)
    
    def update_xm(self, xm):
        self.M, _ = xm.shape
        self.xm = xm
        self.Kmd = self.cov_func(self.xm, self.xd.T, self.cov_params) 