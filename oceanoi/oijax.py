import jax.numpy as jnp
from jax import jit
from jax import random as jrandom
import jax.scipy.linalg as jla
from jax import config

import numpy as np


from .oi import OptimalInterp

config.update("jax_enable_x64", True)
twopi = 2*jnp.pi

class OptimalInterpJax(OptimalInterp):
    """
    Optimal interpolation using Jax to do the heavy lifting
    
    """
    
    def __init__(self, xd, xm, sd, cov_func, cov_params, **kwargs):
        
        OptimalInterp.__init__(self, xd, xm, sd, cov_func, cov_params, **kwargs)
        
        if self.mean_func is not None:
            self.mean_func = self.mean_func
        
        self.key = jrandom.PRNGKey(0)
        
    def __call__(self, yd):
        
        assert yd.shape[0] == self.N, ' first dimension in input data must equal '
        
        alpha = jla.cho_solve((self.L, True), yd - self.mu_d)
        
        return self.mu_m + self.Kmd.dot(alpha)
    
    def prior(self, samples=1, noise=0.):
        return self.sample_prior(samples, noise=noise)
    
    def conditional(self, yd, samples=1):
        return self.sample_posterior(yd, samples)
    
    def log_marg_likelihood(self, yd):
                
        logdet = 2*jnp.sum(jnp.log(jnp.diagonal(self.L)))
        
        alpha = jla.cho_solve((self.L, True), yd - self.mu_d)
        
        qdist = jnp.dot( (yd-self.mu_d).T, alpha)[0,0]
        
        fac = self.N * jnp.log(twopi)
        
        return -0.5*(logdet + qdist + fac)

         
    def _calc_cov(self, cov_func, cov_params):
        # Compute the covariance functions
        Kmd = cov_func(self.xm, self.xd.T, cov_params, *self.cov_args, **self.cov_kwargs)
        Kdd = cov_func(self.xd, self.xd.T, cov_params, *self.cov_args,  **self.cov_kwargs)
        
        return Kmd, Kdd
    
    def _calc_weights(self, Kdd, sd, Kmd):
         
        # Calculate the cholesky factorization
        noise = (sd*sd + 1e-7)*jnp.eye(Kdd.shape[0])
        L = jla.cholesky(Kdd+noise, lower=True)
        w_md = None

        return L, w_md

    def _calc_err(self, diag=True):

        Kmm = self.cov_func(self.xm, self.xm.T, self.cov_params, *self.cov_args, **self.cov_kwargs)
        Kdm = self.cov_func(self.xd, self.xm.T, self.cov_params, *self.cov_args, **self.cov_kwargs)
        
        v = jla.cho_solve((self.L, True),  Kdm)
        
        V = Kmm - v.T.dot(Kdm)
        
        if diag:
            return jnp.diag(V)
        else:
            return V
            
    def sample_posterior(self, yd, samples):
        
        # Predict the mean
        ymu = self.__call__(yd)

        # Predict the covariance
        Σ = self._calc_err(diag=False)
        
        myrand = jrandom.normal(self.key, shape=(self.M,samples))
        jitter = 1e-7*jnp.eye(Σ.shape[0])
        L = jla.cholesky(Σ+jitter, lower=True)
        
        return ymu + L.dot(myrand)
        
        
    def sample_prior(self, samples, noise=0.):
        
        myrand = jrandom.normal(self.key, shape=(self.N,samples)) 
        
        return self.mu_d + self.L.dot(myrand) + noise*myrand

    