import numpy as np
from scipy import sparse

import dask
import dask.array as da
from sksparse.cholmod import cholesky

from .oi import OptimalInterp1D
from .mean import zero 

###
# Sparse matrix construction routines

def cov_on_chunked_1d(x1, x2, covfunc, covparams, threshold):
    K = covfunc(x1[:,None], x2[:,None].T, covparams)
    idx = np.abs(K)>threshold
    R,C = np.where(idx)
    return R, C, K[idx]

def create_cov_chunked(x1, x2, covfunc, covparams, threshold, chunksize, chunkedfunc):
    
    numrows = x1.shape[0]
    numcols = x2.shape[0]
    rows = []
    cols = []
    data = []
    for r in range(0, numrows+chunksize, chunksize):
        
        for c in range(0, numcols+chunksize, chunksize):
            #print(r, c, numrows)
            r1 = r + chunksize
            c1 = c + chunksize
            chunk1 = x1[r:r1]
            chunk2 = x2[c:c1]
            
            r_local, c_local, data_local = chunkedfunc(chunk1, chunk2, covfunc, covparams, threshold)
            r_global = r_local + r
            c_global = c_local + c
            
            rows.append(r_global)
            cols.append(c_global)
            data.append(data_local)
            
    return sparse.csc_matrix((np.hstack(data), (np.hstack(rows), np.hstack(cols))), 
                shape=(numrows, numcols))

### Dask (parallel)routines
@dask.delayed(pure=True)
def cov_on_chunked_dask(x1, x2, covfunc, covparams, threshold):
    K = covfunc(x1[:,...], x2[:,...], covparams)
    idx = np.abs(K)>threshold
    R,C = np.where(idx)
    
    return sparse.csc_matrix((K[idx], (R,C)), 
                shape=K.shape)


def create_cov_chunked_dask(x1, x2, covfunc, covparams, threshold, chunksize, chunkedfunc):
    
    numrows = x1.shape[0]
    numcols = x2.shape[0]
    rows = []
    for r in range(0, numrows+chunksize, chunksize):
        cols = []
        for c in range(0, numcols+chunksize, chunksize):
            r1 = r + chunksize
            c1 = c + chunksize
            chunk1 = x1[r:r1,...]
            chunk2 = x2[c:c1,...]
            
            N,M = chunk1.shape[0], chunk2.shape[0]
            
            delayed_array = chunkedfunc(chunk1, chunk2, covfunc, covparams, threshold)
            
            cols.append(da.from_delayed(
                delayed_array,
                dtype='float',
                shape=(N,M),
            ))
            
        rows.append(da.hstack(cols))
        
    res = da.vstack(rows).compute()
    #res = sparse.triu(res, k=1)
    return res.tocsc()

####
# Optimal interpolation classes

class OptimalInterpSparse(object):
    """
    Optimal interpolation in one dimension (usually time)
    
    """
    
    chunksize=1000
    threshold=1e-15
    #chunkedcovfunc = cov_on_chunked_1d
    
    mean_func = zero
    mean_params = None
    
    def __init__(self, xd, xm, sd, cov_func, cov_params, **kwargs):
        
        self.__dict__.update(kwargs)
        
        assert xd.ndim==2
        
        self.N, self.D = xd.shape
        self.M, D = xm.shape
        
        # Chunks must divide up the array exactly
        assert self.N%self.chunksize==0

        self.xd = xd
        self.xm = xm
        
        self.sd = sd
        self.cov_func = cov_func
        self.cov_params = cov_params
        
        # Evaluate the covariance functions
        self.Kmd, self.Kdd = self._calc_cov(cov_func, cov_params)
        
        # Evaluate the mean function
        self.mu_d = self.mean_func(self.xd, self.mean_params)
        self.mu_m = self.mean_func(self.xm, self.mean_params)
        
        # Calculate the cholesky of Kdd for later use
        self.L, self.w_md = self._calc_weights(self.Kdd, self.sd, self.Kmd)
       
        
    def __call__(self, yd):
        
        assert yd.shape[0] == self.N, ' first dimension in input data must equal '
       
        #return self.w_md.T.dot(yd[:,:])
    
        v_tmp = self.L.solve_L(yd - self.mu_d, use_LDLt_decomposition=False)
        alpha = self.L.solve_Lt(v_tmp, use_LDLt_decomposition=False)
        
        return self.Kmd.dot(alpha)
        
    
    def _calc_cov(self, cov_func, cov_params):
        # Compute the covariance functions
        Kmd = create_cov_chunked_dask(self.xm, self.xd,\
                self.cov_func, self.cov_params, self.threshold, self.chunksize, cov_on_chunked_dask)
        
        Kdd = create_cov_chunked_dask(self.xd, self.xd,\
                self.cov_func, self.cov_params, self.threshold, self.chunksize, cov_on_chunked_dask)

        return Kmd, Kdd
    
    def _calc_weights(self, Kdd, sd, Kmd):
        
        N = self. N
        # Calculate the cholesky factorization
        sigI = sparse.spdiags(1e-7*np.ones(N,),0,N,N)
        noise = sparse.spdiags(sd**2*np.ones(N,),0,N,N)
        factor = cholesky(Kdd + noise + sigI, ordering_method='natural')
        
        #w_tmp = factor.solve_L(self.Kmd.T, use_LDLt_decomposition=False)
        #w_md = factor.solve_Lt(w_tmp, use_LDLt_decomposition=False)
        w_md = sparse.csc_matrix(([0],([0],[0])), shape=(self.M, self.N))

        return factor, w_md

    def calc_err(self, diag=True):
        
        Kmm = create_cov_chunked_dask(self.xm, self.xm,\
                self.cov_func, self.cov_params, self.threshold, self.chunksize, cov_on_chunked_dask)
        
        Kdm = create_cov_chunked_dask(self.xd, self.xm,\
                self.cov_func, self.cov_params, self.threshold, self.chunksize, cov_on_chunked_dask)
        
        #V = Kmm - self.w_md.T.dot(Kdm)
        v_tmp = self.L.solve_L(Kdm, use_LDLt_decomposition=False)
        v = self.L.solve_Lt(v_tmp, use_LDLt_decomposition=False)
        
        V = Kmm - v.T.dot(Kdm)
            
        if diag:
            return V.diagonal()
        else:
            return V
        
    def sample_posterior(self, yd, samples):
        
        print('Warning this sampling may blowout your memory!!')
        # Predict the mean
        ymu = self.__call__(yd)

        # Predict the covariance
        Σ = self.calc_err(diag=False)
        
        L = cholesky(Σ, ordering_method='natural')
        myrand = np.random.normal(size=(self.M,samples)) 
        return  ymu + L.L.dot(myrand)
        
        #return self.mu_m +np.random.multivariate_normal(ymu.ravel(), Σ, samples)
        
        #return np.random.multivariate_normal(ymu.ravel(), Σ, samples)
    
    def sample_prior(self, samples):
        
        #return np.random.multivariate_normal(np.zeros((self.N,)), self.Kdd, samples)
        myrand = np.random.normal(size=(self.N,samples)) 
        return self.mu_d + self.L.L().dot(myrand)

    
    def log_marg_likelihood(self, yd):
        
        logdet = 2*self.L.logdet()
        
        v_tmp = self.L.solve_L(yd-self.mu_d, use_LDLt_decomposition=False)
        v = self.L.solve_Lt(v_tmp, use_LDLt_decomposition=False)
        
        qdist = np.dot((yd-self.mu_d).T, v)[0,0]
        
        fac = self.N * np.log(2*np.pi)
        
        return -0.5*(logdet + qdist + fac)
    
    def print_matrix_sizes(self):
        sizestr = 'N = {}, M = {}\n----'.format(self.N, self.M)
        sizestr += '\n\tKdd = {} ({} MB)'.format(self.Kdd.nnz, self.Kdd.nnz*8/1e6)
        sizestr += '\n\tKmd = {} ({} MB)'.format( self.Kmd.nnz, self.Kmd.nnz*8/1e6)
        sizestr += '\n\tw_md [{}, {}] = {} ({} MB)'.format(self.w_md.shape[0], self.w_md.shape[1],
                                        self.w_md.nnz, self.w_md.nnz*8/1e6)

        print(sizestr)      
            