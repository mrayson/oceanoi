"""
Variogram estimation functions

1-D variogram:

$$
\gamma(h) = \frac{1}{2N(h)} \sum_i^{N(h)} [z(x_i) - z(x_i + h)]^2
$$

Steps:
 1. Compute the distance of vector $X$ with itself i.e. `h = X - X.T`
 2. Find the lags using `np.digitize`

For each lag value:
 3. Compute the number of points $N(h)$
 4. Compute the variogram value

2-D variogram:

$$
\gamma(h,l) = \frac{1}{2N(h,l)} \sum_i^{N(h,l)} [z(x_i,y_i) - z(x_i + h, y_i+l)]^2
$$

Relationship between variogram and covariance:
$$
C(h) = sill - \gamma(h)
$$
"""

import numpy as np

def calc_variogram_1d(X, Z, xbins):
    """
    Calculate variogram parameters along 1-dimension
    """
    nx = X.shape
    xall = np.repeat(X[:,None],nx,axis=1)
    xdist = xall - xall.T # Distance between all points in a vector with every other point

    h = 0.5*(bins[0:-1] + bins[1:])
    nbins = h.shape[0]
    v = np.zeros_like(h)
    N = np.zeros_like(h)

    for ii in range(nbins):
        idx = (xdist>=bins[ii]) & (xdist <=bins[ii+1])
        rc = np.argwhere(idx)
        v[ii] = np.sum( np.power(Z[rc[:,0]]-Z[rc[:,1]],2) )
        N[ii] = idx.sum()
    
    return h, v/(2*N), N

def calc_variogram_2d(X, Y, Z, xbins, ybins):
    """
    Calculate variogram parameters along 2-dimension
    """
    nx = X.shape
    ny = Y.shape
    
    assert nx == ny

    xall = np.repeat(X[:,None],nx,axis=1)
    yall = np.repeat(Y[:,None],nx,axis=1)

    xdist = xall - xall.T # Distance between all points in a vector with every other point
    ydist = yall - yall.T # Distance between all points in a vector with every other point

    hx = 0.5*(xbins[0:-1] + xbins[1:])
    nxbins = hx.shape[0]
    hy = 0.5*(ybins[0:-1] + ybins[1:])
    nybins = hy.shape[0]

    v = np.zeros((nybins,nxbins))
    N = np.zeros((nybins,nxbins))
    # Load 
    for ii in range(nxbins):
        for jj in range(nybins):
            #print(ii,nxbins,jj,nybins)
            idx = (xdist>=xbins[ii]) & (xdist <=xbins[ii+1]) &\
                (ydist>=ybins[jj]) & (ydist <=ybins[jj+1])

            rc = np.argwhere(idx)
            v[jj,ii] = np.sum( np.power(Z[rc[:,0]]-Z[rc[:,1]],2) )
            N[jj,ii] = idx.sum()
    
    return hx, hy, v/(2*N), N

def calc_variogram_3d(X, Y, T, Z, xbins, ybins, tbins):
    """
    Calculate variogram parameters along 3-dimension
    """
    nx = X.shape
    ny = Y.shape
    nt = T.shape
    
    assert nx == ny, ny == nt

    xall = np.repeat(X[:,None],nx,axis=1)
    yall = np.repeat(Y[:,None],nx,axis=1)
    tall = np.repeat(T[:,None],nt,axis=1)


    xdist = xall - xall.T # Distance between all points in a vector with every other point
    ydist = yall - yall.T # Distance between all points in a vector with every other point
    tdist = tall - tall.T # Distance between all points in a vector with every other point

    hx = 0.5*(xbins[0:-1] + xbins[1:])
    nxbins = hx.shape[0]
    hy = 0.5*(ybins[0:-1] + ybins[1:])
    nybins = hy.shape[0]
    ht = 0.5*(tbins[0:-1] + tbins[1:])
    ntbins = ht.shape[0]

    v = np.zeros((nybins,nxbins,ntbins))
    N = np.zeros((nybins,nxbins,ntbins))
    # Load 
    for ii in range(nxbins):
        print(ii, nxbins)
        for jj in range(nybins):
            for tt in range(ntbins):
                idx = (xdist>=xbins[ii]) & (xdist <=xbins[ii+1]) &\
                    (ydist>=ybins[jj]) & (ydist <=ybins[jj+1]) &\
                    (tdist>=tbins[tt]) & (tdist <=tbins[tt+1]) 
                rc = np.argwhere(idx)
                #print(idx.shape)
                v[jj,ii,tt] = np.sum( np.power(Z[rc[:,0]]-Z[rc[:,1]],2) )
                N[jj,ii,tt] = idx.sum()
    
    return hx, hy, ht, v/(2*N), N