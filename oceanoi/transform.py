"""
Coordinate transformation functions
"""
import numpy as np

def rotate_xy(xin, yin, thetadeg):
    theta = np.pi*thetadeg/180
    cost = np.cos(theta)
    sint = np.sin(theta)
    return xin*cost - yin*sint, xin*sint + yin*cost

def get_Rx(phi):
    return np.array([[1., 0., 0.],
                   [0., np.cos(phi), -np.sin(phi)],
                   [0., np.sin(phi), np.cos(phi)]])

def get_Ry(theta):
    return np.array([[np.cos(theta), 0., np.sin(theta)],
                   [0, 1., 0.],
                   [-np.sin(theta), 0., np.cos(theta)]])

def get_Rt(psi):
    return np.array([[np.cos(psi), -np.sin(psi), 0.],
                  [np.sin(psi), np.cos(psi),0.],
                   [0., 0., 1.]])
    
def rotate_3d(X, Y, T, phi_t, theta_y, psi_x):
    
    if isinstance(X,float):
        XYT = np.array([X, Y, T])
        sz=(1,)
    else:
        XYT = np.array([X.ravel(), Y.ravel(), T.ravel()])
        sz = np.shape(X)
    
    
    # N.B. @ is shorthand for np.matmul
    R = get_Rt(phi_t) @ get_Ry(theta_y) @ get_Rx(psi_x)

    # Do the rotation
    XYT_r =  R @ XYT
    
    if isinstance(X, float):
        xr = XYT_r[0]
        yr = XYT_r[1]
        tr = XYT_r[2]
    else:
        xr = XYT_r[0,:].reshape(sz)
        yr = XYT_r[1,:].reshape(sz)
        tr = XYT_r[2,:].reshape(sz)
    
    return xr, yr, tr