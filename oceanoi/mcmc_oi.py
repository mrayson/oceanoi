"""
MCMC parameter estimation using emcee
"""

import numpy as np
import emcee
from .oidask import OptimalInterp


    
def _minfunc_prior( params, x, Z, covfunc, meanfunc, 
           ncovparams, verbose, mean_kwargs, OIclass, oi_kwargs,
           priors):

    noise = params[0]
    covparams = params[1:ncovparams]
    meanparams = params[ncovparams:]
    
    ## Add on the priors
    log_prior = np.array([P.logpdf(val) for P, val in zip(priors, params)])
    if np.any(np.isinf(log_prior)):
        return -np.inf
    sum_prior = np.sum(log_prior)
    #sum_prior = 0.
    
    myOI = OIclass(x, x, noise, covfunc, covparams, mean_func=meanfunc,
                        mean_params=meanparams, mean_kwargs=mean_kwargs, **oi_kwargs)
    
    logp = myOI.log_marg_likelihood(Z)  
    
    return logp + sum_prior


def mcmc_oi(
    xd, yd, 
    covfunc, 
    meanfunc, 
    priors,
    ncovparams,
    mean_kwargs={},
    OIclass=OptimalInterp,
    verbose=False,
    nwalkers=200, nwarmup=200, niter=20, nprior=500,
    oi_kwargs={},
    parallel=True):
    """
    Main MCMC function
    """
    
    if parallel:
        import os 
        os.environ["OMP_NUM_THREADS"] = "1"
        os.environ["MKL_NUM_THREADS"] = "1"
        from multiprocessing import Pool

    ndim = len(priors)

    p0 = [np.array([pp.rvs() for pp in priors]) for i in range(nwalkers)]
    
    if parallel:
        with Pool() as pool:

            sampler = emcee.EnsembleSampler(nwalkers, ndim, 
                                _minfunc_prior, args=(xd, yd, covfunc, meanfunc, 
                                ncovparams, verbose, mean_kwargs, 
                                OIclass, oi_kwargs,
                                priors),
                             pool=pool)

            print("Running burn-in...")
            p0, _, _ = sampler.run_mcmc(p0, nwarmup, progress=True)
            sampler.reset()

            print("Running production...")
            pos, prob, state = sampler.run_mcmc(p0, niter, progress=True)
    else:
        sampler = emcee.EnsembleSampler(nwalkers, ndim, 
                                _minfunc_prior, args=(xd, yd, covfunc, meanfunc, 
                                ncovparams, verbose, mean_kwargs, 
                                OIclass, oi_kwargs,
                                priors),
                             )

        print("Running burn-in...")
        p0, _, _ = sampler.run_mcmc(p0, nwarmup, progress=True)
        sampler.reset()

        print("Running production...")
        pos, prob, state = sampler.run_mcmc(p0, niter, progress=True)
        
    
    samples = sampler.chain[:, :, :].reshape((-1, ndim))
    
    # Output priors
    p0 = np.array([np.array([pp.rvs() for pp in priors]) for i in range(nprior)])
    
    return samples, p0, sampler