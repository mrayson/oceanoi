"""
Covariance functions for optimal interpolation
"""

import numpy as np
from scipy.optimize import minimize
from scipy.special import kv as K_nu
from scipy.special import gamma

from .transform import rotate_xy, rotate_3d

####
# 1D Covariance models (these are used as building blocks for 2d/3d functions)

def expquad_1d(x, xpr, params):
    eta, l = params
    return eta**2. * expquad(x, xpr, l)

def matern32_1d(x, xpr, params):
    eta, l = params
    return eta**2. * matern32(x, xpr, l)

def matern52_1d(x, xpr, params):
    eta, l = params
    return eta**2. * matern52(x, xpr, l)

def cosine_1d(x, xpr, params):
    eta, l = params
    return eta**2. * cosine(x, xpr, l)

def periodic_1d(xi, xj, params):
    eta, l, p = params
    
    d = np.abs(xi-xj)
    sin1 = np.sin(np.pi*d/p)
    sin2 = sin1*sin1
    
    l2 = l*l
    cff = -2/l2
    return eta**2*np.exp(cff*sin2)

def matern_general_1d(x, xpr, params):
    eta, nu, l = params
    dx = np.sqrt((x-xpr)*(x-xpr))
    return matern_general(dx, eta, nu, l)

### Raw functions

def matern52(x,xpr,l):
    fac1 = 5*(x-xpr)*(x-xpr)
    fac2 = np.sqrt(fac1)
    return (1 + fac2/l + fac1/(3*l*l) )*np.exp(-fac2/l)

def matern32(x,xpr,l):
    fac1 = 3*(x-xpr)*(x-xpr)
    fac2 = np.sqrt(fac1)
    return (1 + fac2/l)*np.exp(-fac2/l)

def cosine(x,xpr,l):
    return np.cos(np.pi*np.abs(x-xpr)/(l*l))

def expquad(x, xpr, l):
    return np.exp(-(x-xpr)*(x-xpr)/(2*l*l))

def expquad2(x, x2, xpr, xpr2, lxy):
    return np.exp(-(x-xpr)*(x2-xpr2)*lxy)

def matern_general(dx, eta, nu, l):
    
    cff1 = np.sqrt(2*nu)*dx/l
    K = np.power(eta, 2.) * np.power(2., 1-nu) / gamma(nu)
    K *= np.power(cff1, nu)
    K *= K_nu(nu,cff1)
    
    K[np.isnan(K)] = np.power(eta, 2.)
    
    return K

### Spectral estimation routines
def matern_spectra(f, eta, nu, l, n=1):
    
    S = np.power(eta,2.) * np.power(2.,n) * np.power(np.pi, 0.5*n) 
    S *= gamma(nu+0.5*n) * np.power(2*nu, nu)
    
    cff1 = gamma(nu)*np.power(l, 2*nu)
    cff2 = 2*nu/l**2 + 4*np.pi**2*f**2
    
    S /= cff1
    S *= np.power(cff2, -(nu+0.5*n))
    
    return S


####
# Variogram optimization routines
def optimize_covariance_model(cov_func, Xin, Yin, datain, Xpt, Ypt, param_guess,
                    method='L-BFGS-B', options={'eps':1e-7}):
    
    """
    Optimize a covariance model parameters 
    """

    soln = minimize(min_covariance_lsq, param_guess,
             args=(datain, cov_func, Xin, Yin, Xpt, Ypt),
                method=method,
                options=options,
             ) 
    
    return soln

def min_covariance_lsq(params, Kdata, cov_func, X, Y, xp, yp):
    """ Returns the sum of the squares"""

    Kguess = cov_func(X,Y,xp,yp, params)    

    return np.sum( (Kdata - Kguess)**2 )

def optimize_covariance_model_3d(cov_func, Xin, Yin, Tin, datain, Xpt, Ypt, Tpt, param_guess,
                    method='L-BFGS-B', bounds=None, options={'eps':1e-7}):
    
    """
    Optimize a covariance model parameters 
    """

    soln = minimize(min_covariance_lsq_3d, param_guess,
             args=(datain, cov_func, Xin, Yin, Tin, Xpt, Ypt, Tpt),
                method=method,
                bounds=bounds,
                options=options,
             ) 
    
    return soln

def min_covariance_lsq_3d(params, Kdata, cov_func, X, Y, T, xp, yp, tp):
    """ Returns the sum of the squares"""

    Kguess = cov_func(X,Y,T,xp,yp,tp, params)    

    return np.sum( (Kdata - Kguess)**2 )

####
# 3D models
def lag_expquad_cov_3d(X, Y, T, x, y, t, params):
    """
    3D rotated exponential quadratic with rotation
    
    Inputs:
        X,Y,T: matrices or vectors of input points
        x,y,t: matrices or vectors of output points
        params: tuple length 4
            eta: scale
            lx: x length scale
            ly: y length scale
            c: translation speed parallel to the rotated y-direction
            thetadeg: rotation angle (degrees CCW of East)
    """
    eta, lx, ly, c, thetadeg = params

    # Rotate the coordinates
    Xr, Yr = rotate_xy(X,Y, thetadeg)
    xr, yr = rotate_xy(x,y, thetadeg)
    
    # Translate the y-coordinates
    Yt = Yr - c*T
    yt = yr - c*t
    
    # Build the covariance matrix
    cov = expquad(Xr,xr,lx) 
    cov *= expquad(Yt,yt,ly)

    return eta**2 * cov

def expquad_cov_3d(X, Y, T, x, y, t, params):
    """
    3D Exponential Quadratic Covariance with rotation
    
    Inputs:
        X,Y,T: matrices or vectors of input points
        x,y,t: matrices or vectors of output points
        params: tuple length 4
            eta: scale
            lx: x length scale
            ly: y length scale
            lxy, lxt, lyt: off-diagonal inverse length scale in each plane
            xoff,yoff,toff: offset scalar for each dimensions
    """
    eta, lx, ly, lt, lxy, lxt, lyt, xoff, yoff, toff  = params
    
    # Build the covariance matrix
    C = expquad(X,x+xoff,lx) 
    C *= expquad(Y,y+yoff,ly)
    C *= expquad(T,t+toff,lt)

    # Rotated components
    C *= expquad2(X,Y,x+xoff,y+yoff,lxy)
    C *= expquad2(X,T,x+xoff,t+toff,lxt)
    C *= expquad2(Y,T,y+yoff,t+toff,lyt)

    return eta**2 * C



def rotated_cov_3d(X, Y, T, x, y, t, params, myfunc=expquad):
    """
    3D Exponential Quadratic Covariance with rotation
    
    Inputs:
        X,Y,T: matrices or vectors of input points
        x,y,t: matrices or vectors of output points
        params: tuple length 4
            eta: scale
            lx: x length scale
            ly: y length scale
            lt: t length scale
            theta_x, theta_y, theta_t: rotation angle (relative to each plane)
    """
    eta, lx, ly, lt, theta_x, theta_y, theta_t  = params
    
    Xr, Yr, Tr = rotate_3d(X, Y, T, theta_x, theta_y, theta_t)
    xr, yr, tr = rotate_3d(x, y, t, theta_x, theta_y, theta_t)

    # Build the covariance matrix
    C = myfunc(Xr,xr,lx) 
    C *= myfunc(Yr,yr,ly)
    C *= myfunc(Tr,tr,lt)

    return eta**2 * C


def rotated_waveprop_cov_3d(X, Y, T, x, y, t, params, myfunc=matern32):
    
    eta, lx, ly, lt,  wavedirn, c = params
    
    cx =  1/c*np.cos(wavedirn)
    cy =  1/c*np.sin(wavedirn)

    thetay =  np.arctan(cx)
    thetax =  -np.arctan(cy)
    thetat =  -wavedirn

    params = (eta, lx, ly, lt, thetat, thetay, thetax)
    return rotated_cov_3d(X, Y, T, x, y, t, params, myfunc=myfunc)


# Wrappers for the above
def rotated_expquad_cov_3d(X, Y, T, x, y, t, params):
    """
    Exponential quadratic
    """
    return rotated_cov_3d(X, Y, T, x, y, t, params, myfunc=expquad)

def rotated_matern52_cov_3d(X, Y, T, x, y, t, params):
    """
    Exponential quadratic
    """
    return rotated_cov_3d(X, Y, T, x, y, t, params, myfunc=matern52)

def rotated_matern32_cov_3d(X, Y, T, x, y, t, params):
    """
    Exponential quadratic
    """
    return rotated_cov_3d(X, Y, T, x, y, t, params, myfunc=matern32)

####
# 2D covariance models

def expquad_cov_matrix_2d(X, Y, T, x, y, t, params):
    """
    2D Exponential Quadratic Covariance with rotation
    
    Inputs:
        X,Y,T: matrices or vectors of input points
        x,y,t: matrices or vectors of output points
        params: tuple length 4
            eta: scale
            lx: x length scale
            ly: y length scale
            lxy: off-diagonal inverse length scale (leads to rotations
            xoff, yoff: offset scalars in x and y dimensions
    """
    eta, lx, ly, lxy, xoff, yoff  = params
    
    
    # Build the covariance matrix
    C = expquad(X,x,lx) 
    C *= expquad(Y,y,ly)
    # Rotated components
    C *= expquad2(X,Y,x+xoff,y+yoff,lxy)

    return eta**2 * C

def expquad_cov_2d(X,Y,x,y, params):
    """
    2D Exponential Quadratic Covariance with rotation
    
    Inputs:
        X,Y: matrices or vectors of input points
        x,y: matrices or vectors of output points
        params: tuple length 4
            eta: scale
            lx: x length scale
            ly: y length scale
            thetadeg: rotation angle (degrees CCW of East)
    """
    eta, lx, ly, thetadeg = params

    # Rotate the coordinates
    Xr, Yr = rotate_xy(X,Y, thetadeg)
    xr, yr = rotate_xy(x,y, thetadeg)
    
    # Build the covariance matrix
    cov = expquad(Xr,xr,lx) 
    cov *= expquad(Yr,yr,ly)

    return eta**2 * cov


def matern_cov_2d(X,Y,x,y, params):
    """
    2D Matern Covariance with rotation
    
    Inputs:
        X,Y: matrices or vectors of input points
        x,y: matrices or vectors of output points
        params: tuple length 4
            eta: scale
            lx: x length scale
            ly: y length scale
            thetadeg: rotation angle (degrees CCW of East)
    """
    eta, lx, ly, thetadeg = params

    # Rotate the coordinates
    Xr, Yr = rotate_xy(X,Y, thetadeg)
    xr, yr = rotate_xy(x,y, thetadeg)
    
    # Build the covariance matrix
    cov = matern52(Xr,xr,lx) 
    cov *= matern52(Yr,yr,ly)

    return eta**2 * cov

def cosine_cov_2d(X,Y,x,y, params):
    """
    Inputs:
        X,Y: matrices or vectors of input points
        x,y: scalar points
    """
    eta, lTx, lTy,  thetadeg = params

    # Rotate the coordinates
    Xr, Yr = rotate_xy(X,Y, thetadeg)
    xr, yr = rotate_xy(x,y, thetadeg)
     
    cov = cosine(Xr,xr,lTx)
    cov *= cosine(Yr,yr,lTy)
    
    return eta**2 * cov


def custom_cov_2d(X,Y,x,y, params):
    """
    Inputs:
        X,Y: matrices or vectors of input points
        x,y: scalar points
    """
    eta, lx, ly, lT, thetadeg = params
    #lx, ly, thetadeg = params

    # Rotate the coordinates
    Xr, Yr = rotate_xy(X,Y, thetadeg)
    xr, yr = rotate_xy(x,y, thetadeg)
    
    # Build the covariance matrix
    cov = matern52(Xr,xr,lx) 
    cov *= matern52(Yr,yr,ly)
    cov *= cosine(Xr,xr,lT)
    return eta * cov



# Utility Functions

