# Optimal interpolation for ocean data

Routines to perform optimal interpolation and estimate covariance models from data

## Installation

## Tutorials

1. Perform 1D interpolation and parameter estimation [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fmrayson%2Foceanoi/HEAD?filepath=examples%2Fexample1_1d_parameter_estimate.ipynb) 

## Documentation

TBC...

---

Matt Rayson

University of Western Australian

June 2020